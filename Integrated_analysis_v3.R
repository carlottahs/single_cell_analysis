# Script for running Integrated analysisi

# based on merged_analysis_new.R which included metadata, barcode, sequence run inclusion etc.

#libraries
library(dplyr)
library(Seurat)
library(ggplot2)
library(biomaRt)


path.data.root = file.path("data")
path.data.root

# loading metadata
metadata <- read.csv(paste(path.data.root,"2022_01_04_GEX_10x_sample-list.csv",sep = "/"))
metadata <- metadata %>% filter(row_number() <= n()-8) # getting rid of the abbreviation explanation

#adding sequence run information
sequencing_run = stringr::str_subset(metadata$RQ., ".+")
sequencing_run= rep(sequencing_run,each=2) #repeat each one twice due to merged cells
metadata$RQ= sequencing_run
metadata = metadata %>% tidyr::separate(RQ, into = c("RQ_GEX","RQ_BCR"), sep = ", ")

#deleting non-existing samples
metadata <- metadata[-c(6,12),] # ugly version
row.names(metadata) <- 1:nrow(metadata) # renumbering so no missing values 6 and 12


# adding more metadata information column
metadata$tmp = metadata$New_Sample_Name
metadata= metadata %>% tidyr::separate(tmp, into = c("disease","area","vaccine"), sep = "_") #split into separate columns
metadata$disease = gsub('[[:digit:]]+', '', metadata$disease) # deleting numbers in disease category


#getting names for later
filenames = metadata$filename
samples <- metadata$New_Sample_Name #sample names
typeofdata = "filtered_feature_bc_matrix" # data of interest for loading


obj_list <- list()
barcode.GEX.list <- list()
#barcode.BCR.list <- list()
gene_use=c()
counter=0


# creating object list
# reading in the data
for(sample in samples){
  counter = counter + 1
  cat(sample, '\n')
  
  # Read in GEX data
  path.data = file.path(path.data.root, paste(filenames[counter], typeofdata, sep="/"))
  path.data
  data <- Read10X(data.dir =path.data)
  obj <- CreateSeuratObject(counts = data, project = sample)
  
  #add useful metadata
  obj$sampleName <- sample
  obj[["percent.mt"]] <- PercentageFeatureSet(object = obj, pattern = "^MT-")
  obj$RQ_GEX = unique(filter(metadata, New_Sample_Name == sample)$RQ_GEX) # sequencing run 
  obj$area = unique(filter(metadata, New_Sample_Name == sample)$area) # Vac or Unv
  obj$disease = unique(filter(metadata, New_Sample_Name == sample)$disease) # BCD, HD, or DC
  obj$vaccine = unique(filter(metadata, New_Sample_Name == sample)$vaccine) # 2nd or booster
  
  # reformat the barcodes
  obj <- RenameCells(obj, new.names = substr(Cells(obj), 1, 16)) # remove the -1s
  obj <- RenameCells(obj, add.cell.id = sample) # add in the sample name to keep unique cell names during combination later  
  
  obj_list[[sample]] = obj # read in all GEX data to a list
  barcode.GEX.list[[sample]] = unlist(lapply(strsplit(as.character(unique(Cells(obj))),"-"),function(x) x[1]))
  
}

# normalize and identify variable features for each dataset independently
obj_list <- lapply(X = obj_list, FUN = function(x) {
  x <- NormalizeData(x)
  x <- FindVariableFeatures(x, selection.method = "vst", nfeatures = 2000)
})

# select features that are repeatedly variable across datasets for integration
features <- SelectIntegrationFeatures(object.list = obj_list)


# Finding integration anchors
anchors<- FindIntegrationAnchors(obj_list, anchor.features = features, dims = 1:20)
print("Finished FindIntegrationAnchors")
saveRDS(anchors, "RData/integrated_anchors.rds")
print("Finished Saving Anchors")

# this command creates an 'integrated' data assay
integrated <- IntegrateData(anchorset = anchors, dims = 1:20)
saveRDS(integrated, "RData/integrated.rds")

# specify that we will perform downstream analysis on the corrected data note that the
# original unmodified data still resides in the 'RNA' assay
DefaultAssay(integrated) <- "integrated"


# Run the standard workflow for visualization and clustering
integrated <- ScaleData(integrated, verbose = FALSE)
# Filtering out IG and TR genes?
integrated <- RunPCA(integrated, verbose = FALSE)
# t-SNE and Clustering
integrated <- RunUMAP(integrated, dims = 1:20) 
integrated <- FindNeighbors(integrated, reduction = "pca", dims = 1:20)
integrated <- FindClusters(integrated, resolution = 0.8)
print("Finished Standard Workflow")
saveRDS(integrated, "RData/integrated_final.rds")
print("Finished Saving Objects")

