# Script running integrated analysis of skin Covid Vaccine samples

# Aim:
# loading all 10X datasets as Seuratobject and adding needed information 

library(dplyr)
library(Seurat)

path.data.root = file.path("data")
path.data.root

# loading metadata
metadata <- read.csv(paste(path.data.root,"2022_01_04_GEX_10x_sample-list.csv",sep = "/"))
metadata <- metadata %>% filter(row_number() <= n()-8) # getting rid of the abbreviation explanation
# add sequencing to all cells
metadata$RQ.= gsub("(.*),.*", "\\1", metadata$RQ.) # removing all BCR RQ numbers
sequencing_run = stringr::str_subset(metadata$RQ., ".+")
sequencing_run= rep(sequencing_run,each=2) #repeat each one twice due to merged cells
metadata$RQ. = sequencing_run
metadata <- metadata[-c(6,12),] # ugly version do better later, see above
row.names(metadata) <- 1:nrow(metadata) # renumbering so no missing values 6 and 12
filenames = metadata$filename
samples <- metadata$New_Sample_Name #sample names
typeofdata = "filtered_feature_bc_matrix" # data of interest for loading

obj_list <- list()
barcode.GEX.list <- list()
#barcode.BCR.list <- list()
gene_use=c()
counter=0


for(sample in samples){
  counter = counter + 1
  cat(sample, '\n') 
  
  # Read in GEX data
  path.data = file.path(path.data.root, paste(filenames[counter], typeofdata, sep="/"))
  path.data
  data <- Read10X(data.dir =path.data)

  obj <- CreateSeuratObject(counts = data, project = sample)
  
  #adding metadata
  RQ <- unique(filter(metadata, New_Sample_Name == sample)$RQ.) # 1, 2 or 3
  obj$sampleName <- sample
  obj$RQ <- RQ
  obj$newSampleName = obj$sampleName
  tmp_meta.data = obj@meta.data %>% tidyr::separate(newSampleName, into = c("disease","area","vaccine"), sep = "_")
  obj@meta.data = tmp_meta.data
  obj[["percent.mt"]] <- PercentageFeatureSet(object = obj, pattern = "^MT-")
  barcode.GEX.list[[sample]] = unlist(lapply(strsplit(as.character(unique(Cells(obj))),"-"),function(x) x[1]))
  
  # normalization and variable feature identification
  obj <- NormalizeData(obj, normalization.method = "LogNormalize", scale.factor = 10000)
  obj <- FindVariableFeatures(obj, selection.method = "vst", nfeatures = 2000)
  
  # save in the appropriate locations
  obj_list[[sample]] = obj # read in all GEX data to a list
  
}

features <- SelectIntegrationFeatures(object.list = obj_list)

obj_list <- lapply(X = obj_list, FUN = function(x) {
  x <- ScaleData(x, features = features, verbose = FALSE)
  x <- RunPCA(x, features = features, verbose = FALSE)
})

anchors<- FindIntegrationAnchors(obj_list, anchor.features = features, dims = 1:20)
print("Finished FindIntegrationAnchors")
path.save.root= "Rdata"
saveRDS(anchors, "RData/integrated_anchors.rds")
print("Finished Saving Anchors")
integrated <- IntegrateData(anchorset = anchors, dims = 1:25)

# Run the standard workflow for visualization and clustering
integrated <- ScaleData(integrated, verbose = FALSE)
# Filtering out IG and TR genes?
integrated <- RunPCA(integrated, verbose = FALSE)
# t-SNE and Clustering
integrated <- RunUMAP(integrated, dims = 1:25) 
integrated <- FindNeighbors(integrated, reduction = "pca", dims = 1:25)
integrated <- FindClusters(integrated, resolution = 0.8)
print("Finished Standard Workflow")
saveRDS(integrated, "RData/integrated_final.rds")
print("Finished Saving Objects")

