# Script running integrated analysis of skin Covid Vaccine samples

# Aim:
# loading all 10X datasets as Seuratobject and adding needed information 

library(dplyr)
library(Seurat)

loaddir <- "data" # specify path on HPC

# loading metadata
metadata <- read.csv(paste(loaddir,"2022_01_04_GEX_10x_sample-list.csv",sep = "/"))
metadata <- metadata %>% filter(row_number() <= n()-8) # getting rid of the abbreviation explanation
# test= metadata %>% filter(!stringr::str_detect(Sequence_Name, 'x')) # not sure why these are not working
# test= metadata %>% filter(!grepl('x', Sequence_Name))
metadata <- metadata[-c(6,12),] # ugly version do better later, see above
row.names(metadata) <- 1:nrow(metadata) # renumbering so no missing values 6 and 12

filespath <- list.files(path=loaddir, full.names = T, pattern="cellranger")
filenames <- list.files(path=loaddir, full.names = F, pattern = "cellranger")
typeofdata = "filtered_feature_bc_matrix" # data of interest for loading

metadata$filename = filenames
new_sample_name= metadata$New_Sample_Name # for renaming to new sample names

## for testing purposes only with partial data
# test_data_set= c(1, 5, 8, 10, 11, 15, 16,22, 27, 31, 42)

list_full = purrr::map(paste(filespath,typeofdata,sep = "/"), Seurat::Read10X) #full version
names(list_full)= new_sample_name #full version
# list_full = purrr::map(paste(filespath[test_data_set],typeofdata,sep = "/"), Seurat::Read10X) #test version
# names(list_full)= new_sample_name[test_data_set] #test version

# paste(names(list_full[1]))

# test= list_full[1]
# names(test)
# project_names = names(list_full)

creating_seurat_object= function(i){
  skin = Seurat::CreateSeuratObject(counts = i, min.cells = 3, min.features = 200) # get this to work: project = project_name
}

list_Seurat = purrr::map(list_full, creating_seurat_object) #creating list of individual Seurat objects

# adding information about the experiment
# immune suppressed, healthy, 2nd or booster...
# 3 main Categories:
# Disease state: BCD (Bcell Depletion), HD (Healthy Donor), DC (Disease Control=not treated)
# Area: Vaccinated or Unvaccinated
# Vaccine #: 2nd or Booster
# also other information available such as disease, age, treatment type ...

# adding new column including all information
info= names(list_Seurat)
counter=1
for (i in names(list_Seurat)) {
  list_Seurat[[i]]$information <- info[counter]
  counter= counter + 1
}
rm(counter)


# QC metrics

# Seurat Steps:
# pre-processing individually
# integrating

# Pre-processing individually
list_Seurat= purrr::map(list_Seurat,Seurat::NormalizeData, normalization.method = "LogNormalize", scale.factor = 10000 )
list_Seurat= purrr::map(list_Seurat, Seurat::FindVariableFeatures, selection.method="vst", nfeatures=2000)
# can run some QC metrics after this


# Integration

# From Seurat manual for large datasets: https://satijalab.org/seurat/articles/integration_large_datasets.html
# anchors <- FindIntegrationAnchors(object.list = bm280k.list, reference = c(1, 2), reduction = "rpca",
#                                   dims = 1:50)
# bm280k.integrated <- IntegrateData(anchorset = anchors, dims = 1:50)

immune.anchors <- FindIntegrationAnchors(object.list = list_Seurat, dims = 1:20, reduction = "rpca")
immune.combined <- IntegrateData(anchorset = immune.anchors, dims = 1:20)
immune.combined_tmp <- immune.combined

DefaultAssay(immune.combined) <- "integrated"

# Run the standard workflow for visualization and clustering
immune.combined <- ScaleData(immune.combined, verbose = FALSE) #features + ??
# Filtering out IG and TR genes?
immune.combined <- RunPCA(immune.combined, npcs = 30, verbose = FALSE)
# t-SNE and Clustering
immune.combined <- RunUMAP(immune.combined, reduction = "pca", dims = 1:20)
immune.combined <- FindNeighbors(immune.combined, reduction = "pca", dims = 1:20)
immune.combined <- FindClusters(immune.combined, resolution = 0.5)

# Adding Metadata
# splitting up information column in the three categories
immune.combined$information2 = immune.combined@meta.data$information
new_column_tmp=tidyr::separate(immune.combined@meta.data,"information2",into=c("disease_state","area","vaccine"),sep="_")
immune.combined@meta.data = new_column_tmp
rm(new_column_tmp)

# do saveRDS instead
#rm(list= ls()[!(ls() %in% c('immune.combined','metadata'))]) # also need others ones for QC, different script?
save.image(file = "integrated_analysis.RData")

# Visualization
# p1 <- DimPlot(immune.combined, reduction = "umap", group.by = "information")
# p2 <- DimPlot(immune.combined, reduction = "umap", label = TRUE)
# p3 <- DimPlot(immune.combined, reduction = "umap", group.by = "vaccine")
# p4 <- DimPlot(immune.combined, reduction = "umap", group.by = "area")
# p5 <- DimPlot(immune.combined, reduction = "umap", split.by = "vaccine")
# p6 <- DimPlot(immune.combined, reduction = "umap", split.by = "area")
# cowplot::plot_grid(p1, p2, p3, p4, p5, p6)


