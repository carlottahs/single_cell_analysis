#!/bin/bash
#SBATCH --job-name=merged_analysis
#SBATCH --nodes 2
#SBATCH --ntasks=4
##SBATCH --cpus-per-task=4
#SBATCH --time=16:00:00
#SBATCH --mem=160G
##SBATCH --mem-per-cpu=100G
#SBATCH --mail-type=FAIL
#SBATCH --mail-type=all
#SBATCH --mail-user=carlotta.schieler@yale.edu
#SBATCH --output=merged_analysis.out
##SBATCH --partition=pi_kleinstein

mem_bytes=$(</sys/fs/cgroup/memory/slurm/uid_${SLURM_JOB_UID}/job_${SLURM_JOB_ID}/memory.limit_in_bytes)
mem_gbytes=$(( $mem_bytes / 1024 **3 ))

echo "Starting at $(date)"
echo "Job submitted to the ${SLURM_JOB_PARTITION} partition, the default partition on ${SLURM_CLUSTER_NAME}"
echo "Job name: ${SLURM_JOB_NAME}, Job ID: ${SLURM_JOB_ID}"
echo "  I have ${SLURM_CPUS_ON_NODE} CPUs and ${mem_gbytes}GiB of RAM on compute node $(hostname)"


cd /gpfs/ysm/project/kleinstein/chs69

module load R/4.1.0-foss-2020b
Rscript merged_analysis.R

