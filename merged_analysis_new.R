# GEX expression analysis script

# now includes all steps together
# new features: better metadata naming and barcode reanming
# all other parameters stay the same including number of PC and resolution

#libraries
library(dplyr)
library(Seurat)
library(ggplot2)
library(biomaRt)

# versiondir <-"~/Documents/Seurat/seurat-analysis" # path of version control
# loaddir <- "~/Documents/Project/data" # specify path on HPC

path.data.root = file.path("data")
path.data.root

# loading metadata
metadata <- read.csv(paste(path.data.root,"2022_01_04_GEX_10x_sample-list.csv",sep = "/"))
metadata <- metadata %>% filter(row_number() <= n()-8) # getting rid of the abbreviation explanation

#adding sequence run information
sequencing_run = stringr::str_subset(metadata$RQ., ".+")
sequencing_run= rep(sequencing_run,each=2) #repeat each one twice due to merged cells
metadata$RQ= sequencing_run
metadata = metadata %>% tidyr::separate(RQ, into = c("RQ_GEX","RQ_BCR"), sep = ", ")

#deleting non-existing samples
metadata <- metadata[-c(6,12),] # ugly version
row.names(metadata) <- 1:nrow(metadata) # renumbering so no missing values 6 and 12


# adding more metadata information column
metadata$tmp = metadata$New_Sample_Name
metadata= metadata %>% tidyr::separate(tmp, into = c("disease","area","vaccine"), sep = "_") #split into separate columns
metadata$disease = gsub('[[:digit:]]+', '', metadata$disease) # deleting numbers in disease category


#getting names for later
filenames = metadata$filename
samples <- metadata$New_Sample_Name #sample names
typeofdata = "filtered_feature_bc_matrix" # data of interest for loading


obj_list <- list()
barcode.GEX.list <- list()
#barcode.BCR.list <- list()
gene_use=c()
counter=0



# reading in the data
for(sample in samples){
  counter = counter + 1
  cat(sample, '\n')

  # Read in GEX data
  path.data = file.path(path.data.root, paste(filenames[counter], typeofdata, sep="/"))
  path.data
  data <- Read10X(data.dir =path.data)
  obj <- CreateSeuratObject(counts = data, project = sample)
  
  #add useful metadata
  obj$sampleName <- sample
  obj[["percent.mt"]] <- PercentageFeatureSet(object = obj, pattern = "^MT-")
  obj$RQ_GEX = unique(filter(metadata, New_Sample_Name == sample)$RQ_GEX) # sequencing run 
  obj$area = unique(filter(metadata, New_Sample_Name == sample)$area) # Vac or Unv
  obj$disease = unique(filter(metadata, New_Sample_Name == sample)$disease) # BCD, HD, or DC
  obj$vaccine = unique(filter(metadata, New_Sample_Name == sample)$vaccine) # 2nd or booster
  
  # reformat the barcodes
  obj <- RenameCells(obj, new.names = substr(Cells(obj), 1, 16)) # remove the -1s
  obj <- RenameCells(obj, add.cell.id = sample) # add in the sample name to keep unique cell names during combination later  
  
  obj_list[[sample]] = obj # read in all GEX data to a list
  barcode.GEX.list[[sample]] = unlist(lapply(strsplit(as.character(unique(Cells(obj))),"-"),function(x) x[1]))
  
}

combined <- merge(obj_list[[1]], y = tail(obj_list, -1))

saveRDS(object = combined, file="RData/combined_COVIDskin_scRNA_new.RData")

# Starting Analysis

# Filtering data
combined <- subset(combined, subset = nFeature_RNA > 200 & percent.mt < 10)
print("Subsetted object")

# standard normalization
print("Starting normalization")
combined<- NormalizeData(combined, normalization.method = "LogNormalize", scale.factor = 10000)


# highly variable features
print("Starting finding Varibale Features")
combined <- FindVariableFeatures(combined, selection.method = "vst", nfeatures = 2000)

print("Starting Scaling Data")
combined<- ScaleData(combined, features = rownames(combined)) #this has to be run on HPC!
print("Finished Scaling Data")

saveRDS(combined,"RData/merged_analysis_combined_after_scaling_new.rds")


# load saved meta pulled from Ensembl 93 using biomaRt
load("QC_features_meta.RData")


# get a vector of IG and TR genes 
# do this by subsetting $gene_biotype (as opposed to basing off gene names)
biotypes_excl = unique(features_meta[["gene_biotype"]])[grepl(pattern="^IG_|^TR_", x=unique(features_meta[["gene_biotype"]]))]
remove.genes = features_meta[["external_gene_name"]][features_meta[["gene_biotype"]] %in% biotypes_excl]

# remove the genes
bool.remove.genes <- combined@assays$RNA@var.features %in% remove.genes
combined@assays$RNA@var.features = combined@assays$RNA@var.features[!bool.remove.genes]                     
cat('After remvoing IG/TR genes, total gene count is: ', length(combined@assays$RNA@var.features), '\n')


# Dimensional reduction
print("Starting PCA")
combined <- RunPCA(combined)

# From testing previously one single dataset, I guess that 20 components will be enough
print("Starting FindNeighbors")
combined <- FindNeighbors(combined, reduction = "pca", dims = 1:20)
print("Starting FindCluster")
combined <- FindClusters(combined, resolution = 0.8) # should rather go for a larger resolution and later merge clusters if needed
print("Starting UMAP")
combined <- RunUMAP(combined, reduction = "pca", dims = 1:20)

saveRDS(combined, "RData/merged_analysis_combined_final_new.rds")
print("Saved final data object.")

