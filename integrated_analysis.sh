#!/bin/bash
#SBATCH --job-name=Integrated_analysis
#SBATCH --cpus-per-task=6
#SBATCH --time=12:00:00
##SBATCH --mem=
#SBATCH --mem-per-cpu=15G
#SBATCH --mail-type=FAIL
#SBATCH --mail-type=all
#SBATCH --mail-user=carlotta.schieler@yale.edu
#SBATCH --output=integrated_analysis.out

mem_bytes=$(</sys/fs/cgroup/memory/slurm/uid_${SLURM_JOB_UID}/job_${SLURM_JOB_ID}/memory.limit_in_bytes)
mem_gbytes=$(( $mem_bytes / 1024 **3 ))

echo "Starting at $(date)"
echo "Job submitted to the ${SLURM_JOB_PARTITION} partition, the default partition on ${SLURM_CLUSTER_NAME}"
echo "Job name: ${SLURM_JOB_NAME}, Job ID: ${SLURM_JOB_ID}"
echo "  I have ${SLURM_CPUS_ON_NODE} CPUs and ${mem_gbytes}GiB of RAM on compute node $(hostname)"


cd /gpfs/ysm/project/kleinstein/chs69

module load R/4.1.0-foss-2020b
Rscript Integrated_analysis.R

