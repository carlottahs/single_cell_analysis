# Script running merged analysis of skin Covid Vaccine samples

# all samples were already loaded and merged in script QC_script.R and saced in combined_COVIDskin_scRNA.RData

#libraries
library(dplyr)
library(Seurat)
library(ggplot2)
library(biomaRt)

# # Setting up parallelization
# library(future)
# plan("multiprocess", workers = 4)
# options(future.globals.maxSize = X) # set this oen if you would want to use it on HPC


# loading merged object
print("loading merged object")
combined <- readRDS("combined_COVIDskin_scRNA.RData")
print("Finished loading merged object")
#adding more metadata information to object: diseases, area and vaccine number
temp_meta= combined@meta.data #get metadata from object
temp_meta= temp_meta %>% tidyr::separate(sampleName, into = c("disease","area","vaccine"), sep = "_") #split into separate columns
temp_meta$disease = gsub('[[:digit:]]+', '', temp_meta$disease) # deleting numbers in disease type category
combined@meta.data = temp_meta #new metadate

rm(temp_meta)


# Filtering data
combined <- subset(combined, subset = nFeature_RNA > 200 & percent.mt < 10)
print("Subsetted object")

# standard normalization
print("Starting normalization")
combined<- NormalizeData(combined, normalization.method = "LogNormalize", scale.factor = 10000)


# highly variable features
print("Starting finding Varibale Features")
combined <- FindVariableFeatures(combined, selection.method = "vst", nfeatures = 2000)

# identify the 10 most highly variable genes
top10 <- head(VariableFeatures(combined), 10)

# plot variable features with labels
options(repr.plot.width = 16, repr.plot.height = 8)
p1 <- LabelPoints(VariableFeaturePlot(combined), points = top10, xnudge = 0, ynudge = 0, repel = TRUE) +
  ggplot2::labs(title = "Variable Features", subtitle = "Top-10 highlighted.") +
  ggplot2::theme(plot.title = element_text(hjust = 0.5),plot.subtitle = element_text(hjust = 0.5)
)
saveRDS(p1, "RData/merged_analysis_feature_plot.rds")
print("Saved RData/merged_analysis_feature_plot.rds ")

print("Starting Scaling Data")
combined<- ScaleData(combined, features = rownames(combined)) #this has to be run on HPC!
print("Finished Scaling Data")

saveRDS(combined,"RData/merged_analysis_combined_after_scaling.rds")
print("Saved RData/merged_analysis_feature_plot.rds ")


# load saved meta pulled from Ensembl 93 using biomaRt
load("QC_features_meta.RData")

# CellRanger will always return the same number of features in the GEX matrix 
# as long as the same 10x reference is used 
# In this case, for the reference I used, it's 33538 features
# dim(features_meta)
#[1] 33538     5

# biotypes of features
# The IG_ and TR_ ones are the ones to exclude
# table(features_meta[["gene_biotype"]])

# get a vector of IG and TR genes 
# do this by subsetting $gene_biotype (as opposed to basing off gene names)
biotypes_excl = unique(features_meta[["gene_biotype"]])[grepl(pattern="^IG_|^TR_", x=unique(features_meta[["gene_biotype"]]))]
remove.genes = features_meta[["external_gene_name"]][features_meta[["gene_biotype"]] %in% biotypes_excl]

# remove the genes
bool.remove.genes <- combined@assays$RNA@var.features %in% remove.genes
combined@assays$RNA@var.features = combined@assays$RNA@var.features[!bool.remove.genes]                     
cat('After remvoing IG/TR genes, total gene count is: ', length(combined@assays$RNA@var.features), '\n')


# Dimensional reduction
print("Starting PCA")
combined <- RunPCA(combined)
p2 <- ElbowPlot(combined, ndims = 30)
saveRDS(p2,"RData/merged_analysis_elbow_plot.rds")
print("Saved ElbowPlot")

# From testing previously one single dataset, I guess that 20 components will be enough
print("Starting FindNeighbors")
combined <- FindNeighbors(combined, reduction = "pca", dims = 1:20)
print("Starting FindCluster")
combined <- FindClusters(combined, resolution = 0.8) # should rather go for a larger resolution and later merge clusters if needed
print("Starting UMAP")
combined <- RunUMAP(combined, reduction = "pca", dims = 1:20)

saveRDS(combined, "RData/merged_analysis_combined_final.rds")
print("Saved final data object.")